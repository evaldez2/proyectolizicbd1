package Vistas;

import Controladores.ame_usuario_controlador;
import Controladores.menuadmincontrolador;
import Modelos.ame_usuario_modelo;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class eliminar_usuario extends javax.swing.JFrame {

    public eliminar_usuario() {
        initComponents();
        controlador();
        setTitle("      Eliminar Usuario");
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
        this.eliminarButton.setCursor(new Cursor(HAND_CURSOR));
        this.cerrarButton.setCursor(new Cursor(HAND_CURSOR));
        this.buscarButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void controlador() {
        ame_usuario_controlador u = new ame_usuario_controlador(this);
        eliminarButton.addActionListener(u);
        buscarButton.addActionListener(u);
        cerrarButton.addActionListener(u);
    }

    public ame_usuario_modelo enviardatos() {
        ame_usuario_modelo m = new ame_usuario_modelo();
        m.setId_usuario(idTextField.getText());
        m.setUsuario(usuarioTextField.getText());
        m.setContraseña(contraseñaTextField.getText());
        m.setTipo_rol(rolComboBox.getSelectedItem().toString());
        m.setNombre(nombreTextField.getText());
        m.setApellido(apellidoTextField.getText());
        return m;
    }

    public void limpiar() {
        idTextField.setText("");
        usuarioTextField.setText("");
        contraseñaTextField.setText("");
        nombreTextField.setText("");
        apellidoTextField.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        apellidoTextField = new javax.swing.JTextField();
        nombreTextField = new javax.swing.JTextField();
        contraseñaTextField = new javax.swing.JTextField();
        usuarioTextField = new javax.swing.JTextField();
        rolComboBox = new javax.swing.JComboBox<>();
        eliminarButton = new javax.swing.JButton();
        buscarButton = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        idTextField = new javax.swing.JTextField();
        cerrarButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Tipo de rol");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, -1, -1));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Contraseña");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, -1, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Nombre");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 160, -1, 20));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Apellido");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, -1, 20));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Usuario");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, -1));
        getContentPane().add(apellidoTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 190, 170, -1));
        getContentPane().add(nombreTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 160, 170, -1));
        getContentPane().add(contraseñaTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 170, -1));
        getContentPane().add(usuarioTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 70, 170, -1));

        rolComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "administrador", "colaborador" }));
        getContentPane().add(rolComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, 170, -1));

        eliminarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btneliminar1.png"))); // NOI18N
        eliminarButton.setActionCommand("Eliminar");
        eliminarButton.setBorderPainted(false);
        eliminarButton.setContentAreaFilled(false);
        eliminarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                eliminarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                eliminarButtonMouseExited(evt);
            }
        });
        getContentPane().add(eliminarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 210, 130, 70));

        buscarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnbuscar.png"))); // NOI18N
        buscarButton.setActionCommand("BuscarE");
        buscarButton.setBorderPainted(false);
        buscarButton.setContentAreaFilled(false);
        getContentPane().add(buscarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 40, 30, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("ID");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 20, -1));
        getContentPane().add(idTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 40, 80, -1));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnexit.png"))); // NOI18N
        cerrarButton.setActionCommand("CEU");
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 0, 20, 20));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ventana_eliminar_usuario.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 340, 280));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void eliminarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_eliminarButtonMouseEntered
        eliminarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btneliminar1efecto.png")));
    }//GEN-LAST:event_eliminarButtonMouseEntered

    private void eliminarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_eliminarButtonMouseExited
        eliminarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btneliminar1.png")));
    }//GEN-LAST:event_eliminarButtonMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(eliminar_usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(eliminar_usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(eliminar_usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(eliminar_usuario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new eliminar_usuario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextField apellidoTextField;
    private javax.swing.JButton buscarButton;
    private javax.swing.JButton cerrarButton;
    public javax.swing.JTextField contraseñaTextField;
    private javax.swing.JButton eliminarButton;
    private javax.swing.JTextField idTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    public javax.swing.JTextField nombreTextField;
    public javax.swing.JComboBox<String> rolComboBox;
    public javax.swing.JTextField usuarioTextField;
    // End of variables declaration//GEN-END:variables
}
