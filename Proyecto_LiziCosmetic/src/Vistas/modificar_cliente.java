package Vistas;

import Controladores.ame_cliente_controlador;
import Controladores.ame_usuario_controlador;
import Controladores.menuadmincontrolador;
import Modelos.ame_cliente_modelo;
import Modelos.ame_usuario_modelo;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class modificar_cliente extends javax.swing.JFrame {

    public modificar_cliente() {
        initComponents();
        controlador();
        setTitle("      Modificar Cliente");
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
        this.modificarButton.setCursor(new Cursor(HAND_CURSOR));
        this.cerrarButton.setCursor(new Cursor(HAND_CURSOR));
        this.buscarButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void controlador() {
        ame_cliente_controlador u = new ame_cliente_controlador(this);
        modificarButton.addActionListener(u);
        buscarButton.addActionListener(u);
        cerrarButton.addActionListener(u);
    }

    public ame_cliente_modelo enviardatos() {
        ame_cliente_modelo m = new ame_cliente_modelo();
        m.setCodigo(idTextField.getText());
        m.setNombre(nombreTextField.getText());
        m.setApellido(apellidoTextField.getText());
        m.setTelefono(telefonoTextField.getText());
        return m;
    }

    public void limpiar() {
        idTextField.setText("");
        nombreTextField.setText("");
        apellidoTextField.setText("");
        telefonoTextField.setText("");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        apellidoTextField = new javax.swing.JTextField();
        nombreTextField = new javax.swing.JTextField();
        modificarButton = new javax.swing.JButton();
        buscarButton = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        idTextField = new javax.swing.JTextField();
        telefonoTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cerrarButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Nombre");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, -1, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Apellido");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 120, -1, 20));
        getContentPane().add(apellidoTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 120, 170, -1));
        getContentPane().add(nombreTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 90, 170, -1));

        modificarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png"))); // NOI18N
        modificarButton.setActionCommand("Modificar");
        modificarButton.setBorderPainted(false);
        modificarButton.setContentAreaFilled(false);
        modificarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                modificarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                modificarButtonMouseExited(evt);
            }
        });
        getContentPane().add(modificarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 190, 130, 70));

        buscarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnbuscar.png"))); // NOI18N
        buscarButton.setActionCommand("Buscar");
        buscarButton.setBorderPainted(false);
        buscarButton.setContentAreaFilled(false);
        getContentPane().add(buscarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 60, 30, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("ID");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 60, 20, -1));
        getContentPane().add(idTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 60, 80, -1));
        getContentPane().add(telefonoTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 150, 170, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Telefono");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, -1, 20));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnexit.png"))); // NOI18N
        cerrarButton.setActionCommand("CMC");
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 0, 20, 20));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ventana_modifica_cliente.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 340, 280));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void modificarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseEntered
        modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1efecto.png")));
    }//GEN-LAST:event_modificarButtonMouseEntered

    private void modificarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseExited
        modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png")));
    }//GEN-LAST:event_modificarButtonMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(modificar_cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(modificar_cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(modificar_cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(modificar_cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new modificar_cliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JTextField apellidoTextField;
    private javax.swing.JButton buscarButton;
    private javax.swing.JButton cerrarButton;
    private javax.swing.JTextField idTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JButton modificarButton;
    public javax.swing.JTextField nombreTextField;
    public javax.swing.JTextField telefonoTextField;
    // End of variables declaration//GEN-END:variables
}
