package Vistas;

import Controladores.ame_cliente_controlador;
import Controladores.ame_usuario_controlador;
import Controladores.menuadmincontrolador;
import Controladores.reporte_controlador;
import Modelos.ame_cliente_modelo;
import Modelos.ame_usuario_modelo;
import Modelos.reporte_modelo;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Reportes extends javax.swing.JFrame {

    public Reportes() {
        initComponents();
        controlador();
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        setTitle("      Reportes");
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        this.reporteButton.setCursor(new Cursor(HAND_CURSOR));
        this.cerrarButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void controlador() {
        reporte_controlador u = new reporte_controlador(this);
        reporteButton.addActionListener(u);
        cerrarButton.addActionListener(u);
    }
    
    public reporte_modelo enviardatos(){
        reporte_modelo m = new reporte_modelo();
        
        m.setReporte(reporteComboBox.getSelectedItem().toString());
        
        return m;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        reporteButton = new javax.swing.JButton();
        cerrarButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        reporteComboBox = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        reporteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btngenerar.png"))); // NOI18N
        reporteButton.setActionCommand("generar_reporte");
        reporteButton.setBorderPainted(false);
        reporteButton.setContentAreaFilled(false);
        reporteButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                reporteButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                reporteButtonMouseExited(evt);
            }
        });
        getContentPane().add(reporteButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 130, 130, 70));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnexit.png"))); // NOI18N
        cerrarButton.setActionCommand("cerrar");
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 0, 20, 20));

        jLabel3.setText("Reportes");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 40, -1, 40));

        reporteComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "VENTAS", "INVENTARIO", "CLIENTES" }));
        getContentPane().add(reporteComboBox, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 50, 180, -1));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ventana_reportes.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 340, 270));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void reporteButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reporteButtonMouseEntered
        reporteButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btngenerarefecto.png")));
    }//GEN-LAST:event_reporteButtonMouseEntered

    private void reporteButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_reporteButtonMouseExited
        reporteButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btngenerar.png")));
    }//GEN-LAST:event_reporteButtonMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Reportes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Reportes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cerrarButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton reporteButton;
    private javax.swing.JComboBox<String> reporteComboBox;
    // End of variables declaration//GEN-END:variables
}
