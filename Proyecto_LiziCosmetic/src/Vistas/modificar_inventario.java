package Vistas;

import Controladores.ame_cliente_controlador;
import Controladores.ame_inventario_controlador;
import Controladores.ame_usuario_controlador;
import Controladores.menuadmincontrolador;
import Modelos.ame_cliente_modelo;
import Modelos.ame_inventario_modelo;
import Modelos.ame_usuario_modelo;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class modificar_inventario extends javax.swing.JFrame {

    public modificar_inventario() {
        initComponents();
        controlador();
        setTitle("      Modificar Inventario");
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
        this.modificarButton.setCursor(new Cursor(HAND_CURSOR));
        this.buscarButton.setCursor(new Cursor(HAND_CURSOR));
        this.cerrarButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void controlador() {
        ame_inventario_controlador u = new ame_inventario_controlador(this);
        modificarButton.addActionListener(u);
        buscarButton.addActionListener(u);
        cerrarButton.addActionListener(u);
    }

    public ame_inventario_modelo enviardatos() {
        ame_inventario_modelo m = new ame_inventario_modelo();
        m.setCodigo(codigoTextField.getText());
        m.setNombre(nombreTextField.getText());
        m.setPrecio(precioTextField.getText());
        m.setCant_total(cantidadtotalTextField.getText());
        return m;
    }
    
     public void limpiar() {
        codigoTextField.setText("");
        nombreTextField.setText("");
        precioTextField.setText("");
        cantidadtotalTextField.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        modificarButton = new javax.swing.JButton();
        buscarButton = new javax.swing.JButton();
        codigoTextField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        nombreTextField = new javax.swing.JTextField();
        cantidadtotalTextField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        precioTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        cerrarButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        modificarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png"))); // NOI18N
        modificarButton.setActionCommand("Modificar");
        modificarButton.setBorderPainted(false);
        modificarButton.setContentAreaFilled(false);
        modificarButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                modificarButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                modificarButtonMouseExited(evt);
            }
        });
        getContentPane().add(modificarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 200, 140, 70));

        buscarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnbuscar.png"))); // NOI18N
        buscarButton.setActionCommand("Buscar");
        buscarButton.setBorderPainted(false);
        buscarButton.setContentAreaFilled(false);
        getContentPane().add(buscarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 50, 30, 20));
        getContentPane().add(codigoTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 50, 110, -1));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Codigo");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, 20));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Nombre");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, -1, 20));
        getContentPane().add(nombreTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 80, 170, -1));

        cantidadtotalTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidadtotalTextFieldKeyTyped(evt);
            }
        });
        getContentPane().add(cantidadtotalTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 140, 170, -1));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Cantidad Total");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, -1, 20));

        precioTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                precioTextFieldKeyTyped(evt);
            }
        });
        getContentPane().add(precioTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 110, 170, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Precio");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, -1, 20));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnexit.png"))); // NOI18N
        cerrarButton.setActionCommand("CMI");
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 0, 20, 20));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ventana_modifica_inventario.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 360, 300));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void modificarButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseEntered
        modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1efecto.png")));
    }//GEN-LAST:event_modificarButtonMouseEntered

    private void modificarButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_modificarButtonMouseExited
        modificarButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnmodificar1.png")));
    }//GEN-LAST:event_modificarButtonMouseExited

    private void precioTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_precioTextFieldKeyTyped
     int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }

    }//GEN-LAST:event_precioTextFieldKeyTyped

    private void cantidadtotalTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidadtotalTextFieldKeyTyped
       int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }

    }//GEN-LAST:event_cantidadtotalTextFieldKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(modificar_inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(modificar_inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(modificar_inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(modificar_inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new modificar_inventario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buscarButton;
    public javax.swing.JTextField cantidadtotalTextField;
    private javax.swing.JButton cerrarButton;
    public javax.swing.JTextField codigoTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JButton modificarButton;
    public javax.swing.JTextField nombreTextField;
    public javax.swing.JTextField precioTextField;
    // End of variables declaration//GEN-END:variables
}
