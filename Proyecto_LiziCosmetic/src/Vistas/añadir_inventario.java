package Vistas;

import Controladores.ame_inventario_controlador;
import Modelos.ame_cliente_modelo;
import Modelos.ame_inventario_modelo;
import java.awt.Cursor;
import static java.awt.Frame.HAND_CURSOR;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class añadir_inventario extends javax.swing.JFrame {

    public añadir_inventario() {
        initComponents();
        controlador();
        setTitle("      Añadir Inventario");
        this.setResizable(false); //EVITAMOS QUE EL JFRAME SE MAXIMIZE
        setIconImage(new ImageIcon(getClass().getResource("/Recursos/ventana.png")).getImage());
        this.setLocationRelativeTo(this);
        this.añadirButton.setCursor(new Cursor(HAND_CURSOR));
        this.cerrarButton.setCursor(new Cursor(HAND_CURSOR));
    }

    public void controlador() {
        ame_inventario_controlador u = new ame_inventario_controlador(this);
        añadirButton.addActionListener(u);
        cerrarButton.addActionListener(u);
    }

    public ame_inventario_modelo enviardatos() {
        ame_inventario_modelo m = new ame_inventario_modelo();
        m.setNombre(nombreTextField.getText());
        m.setPrecio(precioTextField.getText());
        m.setCant_total(cantidadtotalTextField.getText());
        return m;
    }

    public void limpiar() {
        nombreTextField.setText("");
        precioTextField.setText("");
        cantidadtotalTextField.setText("");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        añadirButton = new javax.swing.JButton();
        nombreTextField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cantidadtotalTextField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        precioTextField = new javax.swing.JTextField();
        cerrarButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setUndecorated(true);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        añadirButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadir.png"))); // NOI18N
        añadirButton.setActionCommand("Añadir");
        añadirButton.setBorderPainted(false);
        añadirButton.setContentAreaFilled(false);
        añadirButton.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                añadirButtonMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                añadirButtonMouseExited(evt);
            }
        });
        getContentPane().add(añadirButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 210, 130, 70));
        getContentPane().add(nombreTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, 170, -1));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Nombre");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, 20));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 255, 255));
        jLabel7.setText("Cantidad Total");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, 20));

        cantidadtotalTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                cantidadtotalTextFieldKeyTyped(evt);
            }
        });
        getContentPane().add(cantidadtotalTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 170, 170, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Precio");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, -1, 20));

        precioTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                precioTextFieldKeyTyped(evt);
            }
        });
        getContentPane().add(precioTextField, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 130, 170, -1));

        cerrarButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnexit.png"))); // NOI18N
        cerrarButton.setActionCommand("CAI");
        cerrarButton.setBorderPainted(false);
        cerrarButton.setContentAreaFilled(false);
        getContentPane().add(cerrarButton, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 0, 20, 20));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Recursos/ventana_añadir_inventario.png"))); // NOI18N
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 340, 300));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void añadirButtonMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_añadirButtonMouseEntered
        añadirButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadirefecto.png")));
    }//GEN-LAST:event_añadirButtonMouseEntered

    private void añadirButtonMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_añadirButtonMouseExited
        añadirButton.setIcon((Icon) new javax.swing.ImageIcon(getClass().getResource("/Recursos/btnañadir.png")));
    }//GEN-LAST:event_añadirButtonMouseExited

    private void precioTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_precioTextFieldKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_precioTextFieldKeyTyped

    private void cantidadtotalTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_cantidadtotalTextFieldKeyTyped
        int i = evt.getKeyChar();
        if ((i < '0' || i > '9')) {
            Toolkit.getDefaultToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_cantidadtotalTextFieldKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(añadir_inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(añadir_inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(añadir_inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(añadir_inventario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new añadir_inventario().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton añadirButton;
    private javax.swing.JTextField cantidadtotalTextField;
    private javax.swing.JButton cerrarButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField nombreTextField;
    private javax.swing.JTextField precioTextField;
    // End of variables declaration//GEN-END:variables
}
