package Controladores;

import Modelos.cargartablas;
import Vistas.Registro_Clientes;
import Vistas.Registro_Clientes_Colaborador;
import Vistas.Registro_usuarios;
import Vistas.añadir_cliente;
import Vistas.añadir_usuario;
import Vistas.eliminar_cliente;
import Vistas.eliminar_usuario;
import Vistas.menuAdmin;
import Vistas.menuColaborador;
import Vistas.modificar_cliente;
import Vistas.modificar_usuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Registro_clientes_colaborador_controlador implements ActionListener {
    private cargartablas ut = new cargartablas();
    private final Registro_Clientes_Colaborador rc;

    public Registro_clientes_colaborador_controlador(Registro_Clientes_Colaborador rc) {
        super();
        this.rc = rc;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                añadir();
                break;
            case "Modificar":
                modificar();
                break;
            case "Eliminar":
                eliminar();
                break;
            case "RefrescarTabla":
                ut.cargartablaclientes(rc.usuariosTable);
                break;
            case "Regresar Al Menu":
                regresar();
                break;
        }
    }

    public void añadir() {
        añadir_cliente au = new añadir_cliente();
        au.setVisible(true);
    }

    public void modificar() {
        modificar_cliente mu = new modificar_cliente();
        mu.setVisible(true);
    }

    public void eliminar() {
        eliminar_cliente eu = new eliminar_cliente();
        eu.setVisible(true);
    }
    
    public void regresar(){
        rc.dispose();
        menuColaborador mc = new menuColaborador();
        mc.setVisible(true);
    }
}
