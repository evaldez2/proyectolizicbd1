package Controladores;

import Modelos.iniciarsesionmodelo;
import Vistas.Inicio_sesion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import ConexionSql.ConexionSqlserver;
import Vistas.menuAdmin;
import Vistas.menuColaborador;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;


public class inciarsesioncontrolador implements ActionListener {

   
    private final Inicio_sesion isv;
    private iniciarsesionmodelo ism;
    ConexionSqlserver cc = new ConexionSqlserver();
    Connection con = cc.getConnection();


    public inciarsesioncontrolador(Inicio_sesion isv) {
        super();
        this.isv = isv;
        ism = new iniciarsesionmodelo();
    }

    
    public iniciarsesionmodelo getIsm() {
        return ism;
    }

    public void setIsm(iniciarsesionmodelo ism) {
        this.ism = ism;
    }

    
    @Override
    public void actionPerformed(ActionEvent e) {
        
        switch (e.getActionCommand()) {
        
            case "iniciarsesion":
                inicio();
                break;
        }
    }

    
    private void inicio() {
        
        ism = isv.obtenerdatos();

        
        String usuario = ism.getUsuario();
        String contraseña = ism.getContraseña();

        
        try {
            
            String sql = " DECLARE @n TINYINT"
                    + " EXEC SELECT_USUARIO '" + usuario + "','" + contraseña + "',@n OUTPUT"
                    + " SELECT @n Codigo";

           
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            
            if (usuario.isEmpty() || contraseña.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Ususario o contraseña vacio");
            } else if (rs.next()) {
                String bandera = rs.getString("Codigo"); 
                if (bandera.equals("0")) {                

                    //MANDAMOS A LLAMAR AL PROCEDIMIENTO ALMACENADO DE SQL
                    String sql1 = "EXEC Tipo_Usuario '" + usuario + "'";

                    //INICIAMOS LA CONEXION CON LA BASE DE DATOS Y ENVIAMOS LA PETICION SQL(QUERY)
                    Statement st1 = con.createStatement();
                    ResultSet rs1 = st1.executeQuery(sql1);
                 
                    if (rs1.next()) {
                         String tipo = rs1.getString("TIPO_ROL");
                        if (tipo.equals("administrador")||tipo.equals("ADMINISTRADOR")) {
                            isv.dispose(); 
                            menuAdmin ma = new menuAdmin(); 
                            ma.setVisible(true);                          
                        } else if(tipo.equals("colaborador")) {
                            isv.dispose();
                            menuColaborador mc = new menuColaborador();
                            mc.setVisible(true);
                        }
                    }
                } else if (bandera.equals("1")) {
                    JOptionPane.showMessageDialog(null, "Usuario o contraseña no existe"); //EN CASO QUE NO EXISTA EL USUARIO SE LE INDICA
                }
            } else {
                JOptionPane.showMessageDialog(null, "No Existe");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
