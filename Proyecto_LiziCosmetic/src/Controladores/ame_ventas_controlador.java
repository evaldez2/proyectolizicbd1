package Controladores;

import ConexionSql.ConexionSqlserver;
import Modelos.ame_ventas_modelo;
import Modelos.cargartablas;
import Vistas.Registro_Ventas;
import Vistas.añadir_venta;
import Vistas.modificar_ventas;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ame_ventas_controlador implements ActionListener {

    ConexionSqlserver cc = new ConexionSqlserver();
    Connection con = cc.getConnection();

    private Registro_Ventas rv = new Registro_Ventas();
    private cargartablas ut = new cargartablas();
    private añadir_venta av;
    private modificar_ventas mv;
    private ame_ventas_modelo ame;

    public ame_ventas_controlador(añadir_venta av) {
        super();
        this.av = av;
        ame = new ame_ventas_modelo();
    }

    public ame_ventas_controlador(modificar_ventas mv) {
        super();
        this.mv = mv;
        ame = new ame_ventas_modelo();
    }

    public ame_ventas_modelo getAme() {
        return ame;
    }

    public void setAme(ame_ventas_modelo ame) {
        this.ame = ame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                añadir();
                break;

            case "Modificar":
                modificar();
                break;

            case "CMV":
                mv.dispose();
                break;

            case "CAV":
                av.dispose();
                break;
        }
    }

    public void añadir() {
        ame = av.enviardatos();
        String idcliente = ame.getCodigocliente();
        String nombrecliente = ame.getNombrecliente();
        String idproducto = ame.getCodigoproducto();
        String nombreproducto = ame.getNombreproducto();
        String cantidad = ame.getCantidad();
        String precio = ame.getPrecio();
        String total = ame.getTotal();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC REGISTRAR_VENTAS '" + idcliente + "','" + nombrecliente + "','" + idproducto + "','" + nombreproducto + "', '" + cantidad + "', '" + precio + "', '" + total + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validador = rs.getString("Codigo");

                if (validador.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Ingresado Correctamente");
                    av.limpiar();
                    //ut.cargartablaventas(rv.ventastotalesTable);
                } else if (validador.equals("1")) {
                    JOptionPane.showMessageDialog(null, "No hay producto en stock");
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void modificar() {
        ame = mv.enviardatos();
        String numerofactura = ame.getNumerofactura();
        String codigoproducto = ame.getCodigoproducto();
        String estado = ame.getEstadoventa();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC ACTUALIZAR_VENTA '" + numerofactura + "','" + codigoproducto + "','" + estado + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validador = rs.getString("Codigo");

                if (validador.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                    mv.limpiar();

                    //ut.cargartablausuario(rv.ventastotalesTable);
                }else if(validador.equals("1")){
                    JOptionPane.showMessageDialog(null, "No se puede cambiar el estado, genere una nueva factura");
                }

            }
        } catch (Exception e) {
            //System.out.println(e.getMessage());
        }
    }

    /*public void setdate() {
        ame = mv.enviardatos();
        String id = ame.getId_usuario();

        try {
            String sql = "SELECT * FROM USUARIOSVIEW WHERE ID_USUARIO = '" + id + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                mv.usuarioTextField.setText(rs.getString("USUARIO"));
                mv.contraseñaTextField.setText(rs.getString("CONTRASEÑA"));
                mv.nombreTextField.setText(rs.getString("NOMBRE"));
                mv.apellidoTextField.setText(rs.getString("APELLIDO"));
                mv.rolComboBox.setSelectedItem(String.valueOf(rs.getString("TIPO_ROL")));
            }
        } catch (Exception e) {
        }
    }*/

 /*public void setdateE() {
        ame = ev.enviardatos();
        String id = ame.getId_usuario();

        try {
            String sql = "SELECT * FROM USUARIOSVIEW WHERE ID_USUARIO = '" + id + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                ev.usuarioTextField.setText(rs.getString("USUARIO"));
                ev.contraseñaTextField.setText(rs.getString("CONTRASEÑA"));
                ev.nombreTextField.setText(rs.getString("NOMBRE"));
                ev.apellidoTextField.setText(rs.getString("APELLIDO"));
                ev.rolComboBox.setSelectedItem(String.valueOf(rs.getString("TIPO_ROL")));
            }
        } catch (Exception e) {
        }
    }*/

 /* public void eliminar() {
        ame = ev.enviardatos();
        String id = ame.getId_usuario();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC ELIMINAR_USUARIO '" + id + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validar = rs.getString("CODIGO");

                if (validar.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }*/
}
