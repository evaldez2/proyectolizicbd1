package Controladores;

import Modelos.cargartablas;
import Vistas.Registro_Inventario;
import Vistas.añadir_inventario;
import Vistas.eliminar_inventario;
import Vistas.menuAdmin;
import Vistas.modificar_inventario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Registro_inventario_controlador implements ActionListener {
    private cargartablas ut = new cargartablas();
    private final Registro_Inventario ri;

    public Registro_inventario_controlador(Registro_Inventario ri) {
        super();
        this.ri = ri;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                añadir();
                break;
            case "Modificar":
                modificar();
                break;
            case "Eliminar":
                eliminar();
                break;
            case "RefrescarTabla":
                ut.cargartablainventario(ri.usuariosTable);
                break;
            case "Regresar Al Menu":
                regresar();
                break;
        }
    }

    public void añadir() {
        añadir_inventario au = new añadir_inventario();
        au.setVisible(true);
    }

    public void modificar() {
        modificar_inventario mu = new modificar_inventario();
        mu.setVisible(true);
    }

    public void eliminar() {
        eliminar_inventario eu = new eliminar_inventario();
        eu.setVisible(true);
    }
    
    public void regresar(){
        ri.dispose();
        menuAdmin ma = new menuAdmin();
        ma.setVisible(true);
    }
}
