package Controladores;

import Modelos.cargartablas;
import Vistas.Registro_usuarios;
import Vistas.añadir_usuario;
import Vistas.eliminar_usuario;
import Vistas.menuAdmin;
import Vistas.modificar_usuario;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Registro_usuarios_controlador implements ActionListener {
    private cargartablas ut = new cargartablas();
    private final Registro_usuarios ru;

    public Registro_usuarios_controlador(Registro_usuarios ru) {
        super();
        this.ru = ru;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                añadir();
                break;
            case "Modificar":
                modificar();
                break;
            case "Eliminar":
                eliminar();
                break;
            case "RefrescarTabla":
                ut.cargartablausuario(ru.usuariosTable);
                break;
            case "Regresar Al Menu":
                regresar();
                break;
        }
    }

    public void añadir() {
        añadir_usuario au = new añadir_usuario();
        au.setVisible(true);
    }

    public void modificar() {
        modificar_usuario mu = new modificar_usuario();
        mu.setVisible(true);
    }

    public void eliminar() {
        eliminar_usuario eu = new eliminar_usuario();
        eu.setVisible(true);
    }
    
    public void regresar(){
        ru.dispose();
        menuAdmin ma = new menuAdmin();
        ma.setVisible(true);
    }
}
