package Controladores;

import ConexionSql.ConexionSqlserver;
import Modelos.ame_cliente_modelo;
import Modelos.cargartablas;
import Vistas.Registro_Clientes;
import Vistas.añadir_cliente;
import Vistas.eliminar_cliente;
import Vistas.modificar_cliente;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class ame_cliente_controlador implements ActionListener {

    ConexionSqlserver cc = new ConexionSqlserver();
    Connection con = cc.getConnection();
    private Registro_Clientes rc = new Registro_Clientes();
    private cargartablas ut = new cargartablas();
    private añadir_cliente ac;
    private modificar_cliente mc;
    private eliminar_cliente ec;
    private ame_cliente_modelo amc;

    public ame_cliente_controlador(añadir_cliente ac) {
        super();
        this.ac = ac;
        amc = new ame_cliente_modelo();
    }

    public ame_cliente_controlador(modificar_cliente mc) {
        super();
        this.mc = mc;
        amc = new ame_cliente_modelo();
    }

    public ame_cliente_controlador(eliminar_cliente ec) {
        super();
        this.ec = ec;
        amc = new ame_cliente_modelo();
    }

    public añadir_cliente getAc() {
        return ac;
    }

    public void setAc(añadir_cliente ac) {
        this.ac = ac;
    }

    public modificar_cliente getMc() {
        return mc;
    }

    public void setMc(modificar_cliente mc) {
        this.mc = mc;
    }

    public eliminar_cliente getEc() {
        return ec;
    }

    public void setEc(eliminar_cliente ec) {
        this.ec = ec;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
            case "Añadir":
                añadir();
                break;
            case "Modificar":
                modificar();
                break;

            case "Eliminar":
                eliminar();
                break;

            case "Buscar":
                setdate();
                break;
            case "BuscarE":
                setdateE();
                break;
            case "CMC":
                mc.dispose();
                break;
            case "CEC":
                ec.dispose();
                break;
            case "CAC":
                ac.dispose();
                break;
        }
    }

    public void añadir() {
        amc = ac.enviardatos();
        String nombre = amc.getNombre();
        String apellido = amc.getApellido();
        String telefono = amc.getTelefono();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC REGISTRAR_CLIENTE '" + nombre + "','" + apellido + "','" + telefono + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validador = rs.getString("Codigo");

                if (validador.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Ingresado Correctamente");
                    ac.limpiar();
                    
                    ut.cargartablaclientes(rc.usuariosTable);
                } else {
                    JOptionPane.showMessageDialog(null, "Ya existe el teléfono");
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void modificar() {
        amc = mc.enviardatos();
        String id = amc.getCodigo();
        String nombre = amc.getNombre();
        String apellido = amc.getApellido();
        String telefono = amc.getTelefono();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC ACTUALIZAR_CLIENTE '" + id + "','" + nombre + "','" + apellido + "','" + telefono + "', @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validador = rs.getString("Codigo");

                if (validador.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Actualizado Correctamente");
                    mc.limpiar();
                    ut.cargartablaclientes(rc.usuariosTable);
                } else {
                    JOptionPane.showMessageDialog(null, "Ya existe el usuario");
                }

            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void setdate() {
        amc = mc.enviardatos();
        String id = amc.getCodigo();

        try {
            String sql = "SELECT * FROM CLIENTESVIEW WHERE ID_CLIENTE = '" + id + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                mc.nombreTextField.setText(rs.getString("NOMBRE"));
                mc.apellidoTextField.setText(rs.getString("APELLIDO"));
                mc.telefonoTextField.setText(rs.getString("Telefono"));

            }
        } catch (Exception e) {
        }
    }

    public void setdateE() {
        amc = ec.enviardatos();
        String id = amc.getCodigo();

        try {
            String sql = "SELECT * FROM CLIENTESVIEW WHERE ID_CLIENTE= '" + id + "'";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                ec.nombreTextField.setText(rs.getString("NOMBRE"));
                ec.apellidoTextField.setText(rs.getString("APELLIDO"));
                ec.telefonoTextField.setText(rs.getString("Telefono"));
            }
        } catch (Exception e) {
        }
    }

    public void eliminar() {
        amc = ec.enviardatos();
        String id = amc.getCodigo();

        try {
            String sql = "DECLARE @A INT;"
                    + "EXEC ELIMINAR_CLIENTE '" + id + "',  @A OUTPUT;"
                    + "SELECT @A CODIGO;";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            if (rs.next()) {
                String validar = rs.getString("CODIGO");

                if (validar.equals("0")) {
                    JOptionPane.showMessageDialog(null, "Eliminado Correctamente");
                    ec.limpiar();
                    
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
