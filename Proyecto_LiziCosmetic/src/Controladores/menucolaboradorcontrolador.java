package Controladores;

import Vistas.Registro_Clientes;
import Vistas.Inicio_sesion;
import Vistas.Registro_Clientes_Colaborador;
import Vistas.Registro_Inventario;
import Vistas.Registro_Ventas;
import Vistas.Registro_Ventas_Colaborador;
import Vistas.Registro_usuarios;
import Vistas.menuColaborador;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//IMPLEMENTAMOS ACTIONLISTENER PARA PODER CONTROLAR LOS BOTONES DE LA VISTA POR SU ACTIONCOMMAND
public class menucolaboradorcontrolador implements ActionListener {
    //INSTANCIAMOS LA VISTA
    private final menuColaborador mc;

    //CREAMOS UN CONSTRUCTOR QUE CONTENGA LA INSTANCIA DE LA VISTA
    public menucolaboradorcontrolador(menuColaborador mc) {
        super();
        this.mc = mc;
    }

    //CREAMOS EL ACTIONPERFORMED PARA PODER CONTROLAR LAS ACCIONES DE LOS BOTONES DE LA VISTA
    @Override
    public void actionPerformed(ActionEvent e) {
        switch (e.getActionCommand()) {
             //CREAMOS LOS CASOS DE LOS BOTONES DONDE LOS NOMBRAMOS POR EL ACTIONCOMMAND
            case "ventas": //MISMO NOMBRE QUE LE PUSIMOS EN EL ACTION COMMAND
                ventas(); //MANDAMOS A LLAMAR LA FUNCION QUE TENEMOS ABAJO CON EL MISMO NOMBRE
                break; //SI SE HA SELECCIONADO VENTAS SE DETENDRA SOLAMENTE EN VENTAS
                
            case "clientes": //MISMO NOMBRE QUE LE PUSIMOS EN EL ACTION COMMAND
                clientes(); //MANDAMOS A LLAMAR LA FUNCION QUE TENEMOS ABAJO CON EL MISMO NOMBRE
                break; //SI SE HA SELECCIONADO CLIENTES SE DETENDRA SOLAMENTE EN CLIENTES
                
            case "inventario": //MISMO NOMBRE QUE LE PUSIMOS EN EL ACTION COMMAND
                inventario(); //MANDAMOS A LLAMAR LA FUNCION QUE TENEMOS ABAJO CON EL MISMO NOMBRE
                break;//SI SE HA SELECCIONADO INVENTARIO SE DETENDRA SOLAMENTE EN INVENTARIO
                
            case "cerrarsesion": //MISMO NOMBRE QUE LE PUSIMOS EN EL ACTION COMMAND
                cerrarsesion(); //MANDAMOS A LLAMAR LA FUNCION QUE TENEMOS ABAJO CON EL MISMO NOMBRE
                break; //SI SE HA SELECCIONADO CERRARSESION SE DETENDRA SOLAMENTE EN CERRARSESION
        }
    }
    
    //CREAMOS LAS FUNCIONES DONDE DARA ACCIONES A REALIZAR Y LUEGO LA MANDAMOS A LOS CASE DE ARRIBA
    public void ventas() {
        mc.dispose(); //CERRAMOS LA VENTANA DEL MENU ADMIN
        Registro_Ventas_Colaborador v = new Registro_Ventas_Colaborador(); //INSTANCIAMOS LA CLASE A USAR (JFRAME)
        v.setVisible(true); // HACEMOS QUE SEA VISIBLE LA CLASE (JFRAME)
    }

    public void clientes() {
        mc.dispose(); //CERRAMOS LA VENTANA DEL MENU ADMIN
        Registro_Clientes_Colaborador c = new Registro_Clientes_Colaborador(); //INSTANCIAMOS LA CLASE A USAR (JFRAME)
        c.setVisible(true); // HACEMOS QUE SEA VISIBLE LA CLASE (JFRAME)
    }

    public void inventario() {
        mc.dispose(); //CERRAMOS LA VENTANA DEL MENU ADMIN
        Registro_Inventario i = new Registro_Inventario(); //INSTANCIAMOS LA CLASE A USAR (JFRAME)
        i.setVisible(true); // HACEMOS QUE SEA VISIBLE LA CLASE (JFRAME)
    }

    public void cerrarsesion() {
        mc.dispose(); //CERRAMOS LA VENTANA DEL MENU ADMIN
        Inicio_sesion is = new Inicio_sesion(); //INSTANCIAMOS LA CLASE A USAR (JFRAME)
        is.setVisible(true); // HACEMOS QUE SEA VISIBLE LA CLASE (JFRAME)
    }
}
