package Modelos;

public class iniciarsesionmodelo {
    //INSTANCIAMOS LOS PARAMETROS DE LA VISTA
    String usuario;
    String contraseña;
    
    //CREAMOS UN CONSTRUCTOR CON DICHOS PARAMETROS
    public iniciarsesionmodelo(String usuario, String contraseña) {
        this.usuario = usuario;
        this.contraseña = contraseña;
    }
    
    //CREAMOS UN CONSTRUCTOR VACIO DE DICHOS PARAMETROS
    public iniciarsesionmodelo() {
        this.usuario = "";
        this.contraseña = "";
    }   
    
    //CREAMOS LOS SET DE LOS PARAMETROS
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }
    
    //CREAMOS LOS GET DE LOS PARAMETROS
    public String getUsuario() {
        return usuario;
    }

    public String getContraseña() {
        return contraseña;
    }   
}
