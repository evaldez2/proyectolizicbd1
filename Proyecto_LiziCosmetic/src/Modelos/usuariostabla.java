/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import ConexionSql.ConexionSqlserver;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Vector;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class usuariostabla {

    ConexionSqlserver cc = new ConexionSqlserver();
    Connection con = cc.getConnection();

    public void cargartablausuario(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM USUARIO ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void cargartablaclientes(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM CLIENTE ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }

    public void cargartablainventario(JTable tabla) {
        DefaultTableModel model = (DefaultTableModel) tabla.getModel();
        TableRowSorter<TableModel> elqueordena = new TableRowSorter<TableModel>(model);
        tabla.setRowSorter(elqueordena);
        model.setRowCount(0);

        try {
            String sql = "SELECT * FROM INVENTARIO ";
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                Vector v = new Vector();
                v.add(rs.getInt(1));
                v.add(rs.getString(2));
                v.add(rs.getString(3));
                v.add(rs.getString(4));
                v.add(rs.getString(5));
                v.add(rs.getString(6));
                model.addRow(v);

                tabla.setModel(model);
            }

        } catch (Exception e) {
            System.out.println("" + e.getMessage());
        }
    }
}
