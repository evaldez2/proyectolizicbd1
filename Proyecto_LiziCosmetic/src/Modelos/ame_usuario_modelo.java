package Modelos;

public class ame_usuario_modelo {

    String id_usuario;
    String usuario;
    String contraseña;
    String tipo_rol;
    String nombre;
    String apellido;

    public ame_usuario_modelo(String id_usuario, String usuario, String contraseña, String tipo_rol, String nombre, String apellido) {
        this.id_usuario = id_usuario;
        this.usuario = usuario;
        this.contraseña = contraseña;
        this.tipo_rol = tipo_rol;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public ame_usuario_modelo() {
        this.id_usuario = "";
        this.usuario = "";
        this.contraseña = "";
        this.tipo_rol = "";
        this.nombre = "";
        this.apellido = "";
    }

    public String getId_usuario() {
        return id_usuario;
    }

    public void setId_usuario(String id_usuario) {
        this.id_usuario = id_usuario;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getTipo_rol() {
        return tipo_rol;
    }

    public void setTipo_rol(String tipo_rol) {
        this.tipo_rol = tipo_rol;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
}
