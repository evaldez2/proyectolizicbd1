package Modelos;

public class ame_inventario_modelo {

    String fecha;
    String codigo;
    String nombre;
    String precio;
    String cant_total;
    String cant_vendida;
    String cantd_disponible;

    public ame_inventario_modelo(String fecha, String codigo, String nombre, String precio,String cant_total, String cant_vendida, String cantd_disponible) {
        this.fecha = fecha;
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
        this.cant_total = cant_total;
        this.cant_vendida = cant_vendida;
        this.cantd_disponible = cantd_disponible;
    }
    
    public ame_inventario_modelo() {
        this.fecha = "";
        this.codigo = "";
        this.nombre = "";
        this.precio = "";
        this.cant_total = "";
        this.cant_vendida = "";
        this.cantd_disponible = "";
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }
    
    

    public String getCant_total() {
        return cant_total;
    }

    public void setCant_total(String cant_total) {
        this.cant_total = cant_total;
    }

    public String getCant_vendida() {
        return cant_vendida;
    }

    public void setCant_vendida(String cant_vendida) {
        this.cant_vendida = cant_vendida;
    }

    public String getCantd_disponible() {
        return cantd_disponible;
    }

    public void setCantd_disponible(String cantd_disponible) {
        this.cantd_disponible = cantd_disponible;
    }
    
    
    
}
