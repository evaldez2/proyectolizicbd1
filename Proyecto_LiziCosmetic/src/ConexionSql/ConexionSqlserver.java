package ConexionSql;

import java.sql.*;

/**
 *
 * @author Eliza
 */
public class ConexionSqlserver {

    //Cadena de texto que me guarde el nombre de mi base de datos
    String nombre_base_de_datos = "LIZICOSMETIC";

    //Cadena de texto que me guarde el usuario de sql server
    String usuario = "Lizi";

    //Cadena de texto que me guarde la contraseña de sql server
    String contraseña = "1234";

    //Cadena de texto que me guarde el nombre del driver de sql server

    String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";

    //Cadena de texto que me enrute la conexion entre java y sql server
   
    String url = "jdbc:sqlserver://localhost:1433;databaseName=" + nombre_base_de_datos + ";integratedSecurity=false";

    //Connection = es para iniciar la conexion entre la base de datos y el java, por medio del driver sql
 
    Connection connection = null;

    //Constructor de la clase ConexionSQLServer
    public ConexionSqlserver() {
        try {
         
            Class.forName(driver);

          
            connection = DriverManager.getConnection(url, usuario, contraseña);

        
            if (connection != null) {
               
            }
        } catch (ClassNotFoundException | SQLException e) {
          
            System.out.println(e.getMessage());
        }
    }

    
    public Connection getConnection() {
        return connection;
    }

    //Cierra la conexion entre java y sql al finalizar una operacion.
    public void desconectar() {
        try {
            //Cerramos la conexion con el sql
            System.out.println("Cerrando Conexion Con SQL");
            connection.close();
        } catch (SQLException e) {
           
            System.out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
        ConexionSqlserver C = new ConexionSqlserver();
        C.getConnection();
    }
}
