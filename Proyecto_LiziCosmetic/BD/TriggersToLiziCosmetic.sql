USE LIZICOSMETIC
GO
--Trigger

--1. Actualizar Inventario Por Compras 

create trigger ActInvC
on 
DetalleCompras
after insert
as
BEGIN
   update Productos set CANTIDAD_DISPONIBLE=CANTIDAD_DISPONIBLE+(select cantc from inserted)
   from inserted dc,productos p where p.CODIGO=dc.Cod_prod

   update Productos set precio=(select precioc from inserted)
   from inserted dc, productos p where p.CODIGO=dc.Cod_prod
   and precioc>=precio
END
GO

   --2.Actualizar Inventario por Devoluciones de CLientes
   
   create trigger ActInvDCE
on 
DetalleDevolucionesClientes
after insert
as
   update Productos set CANTIDAD_DISPONIBLE=CANTIDAD_DISPONIBLE+(select cantdce from inserted)
   from inserted dc,productos p where p.CODIGO=dc.Cod_prod


--3.Actualizar Inventario por devoluciones a Proveedores 
create trigger ActInvDP
on 
DetallesDevolucionesProveedores
after insert
as
   update Productos set CANTIDAD_DISPONIBLE=CANTIDAD_DISPONIBLE-(select cantdp from inserted)
   from inserted dc,productos p where p.CODIGO=dc.Cod_prod
